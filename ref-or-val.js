'use strict'

var obj = {
	first: 'yaboi',
	last: 'otru'
}

var arr = [1,2,3]

// strings are immutable. can change by substring method and concatenation.
var str = 'nintendo64'

var a = 10

function changeStuff(obj, arr, str, a){
	obj.first = 'osnap'
	arr[0] = 5
	str[0] = 'b'
	a = 20
}

changeStuff(obj, arr, str, a)

console.log(obj)
console.log(arr)
console.log(str)
console.log(a)