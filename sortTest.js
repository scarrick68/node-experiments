// seeing what happens when sorting differents strings
// and numbers with default sort() method as well as
// using a compare function with sort()

var strs = ['apples', 'cucumbers', 'dates', 'bananas', 'cheerios']
var nums = [10, 3, 7, 20, 15, 8, 7, 9]
// NOTE: you don't need to assign the result to a var.
// sort() modifies the original array
console.log('**** array of strings default sort ****')
console.log(strs)
strs.sort()
console.log('array of strings sorted without compare function')
console.log(strs)

// this is pretty fucked up because the algorithm looks at the first
// digit and says hey, one and two are smaller than 3, so 10, 15 and 20
// all get put in front of 3, 7, 8 and 9
// NOTE: almost always want to use compare function for numbers
console.log('**** array of nums default sort ****')
console.log(nums)
nums.sort()
console.log('array of numbers sorted without compare function')
console.log(nums)

// This compare function will sort them in ascending order
// if a = 40 and b = 100 then a - b = -60 and the algorithm
// knows 40 is less than 100. Reverse them and get 60, which
// means 100 > 40 and it still works out.
console.log('**** array of nums ascending sort ****')
console.log(nums)
nums.sort(function(a, b){
	return a - b;
})
console.log('ascending array of numbers')
console.log(nums)

// Use this compare function to sort the numbers in descending order
console.log('**** array of nums descending sort ****')
console.log(nums)
nums.sort(function(a, b){
	return a + b;
})
console.log('descending array of numbers')
console.log(nums)