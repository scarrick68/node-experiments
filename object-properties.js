// Properties (enumerable and non-enumerable) of empty objects created in different ways
// same result for all of them symbols: [], properties: []
var obj = {}
var properties = Object.getOwnPropertyNames(obj)
var symbols = Object.getOwnPropertySymbols(obj)

console.log('symbols', symbols)
console.log('properties', properties)

obj = Object.create(Object.prototype)
var properties = Object.getOwnPropertyNames(obj)
var symbols = Object.getOwnPropertySymbols(obj)

console.log('symbols', symbols)
console.log('properties', properties)

obj = new Object()
var properties = Object.getOwnPropertyNames(obj)
var symbols = Object.getOwnPropertySymbols(obj)

console.log('symbols', symbols)
console.log('properties', properties)
console.log(obj.length)									// undefined
console.log(Object.length)								// 1, but why?

// Testing equality
// two objects with the same properties will return false
// comparing the same object against itself returns true
// JS must be checking whether the pointer to the objects
// is the same. different objects, different pointers.
var obj1 = {}
var obj2 = {}
var equal = Object.is(obj1, obj2)
console.log('{} and {} are equal?', equal)
equal = Object.is(obj1, obj1)
console.log('obj1 and obj1 equal', equal)
