var now = require("../node_modules/performance-now")

// declare vars inside loop
function varInside(){
	for(var i = 0; i < 100000000; i++){
		var temp = i;
		var temp2 = i + 1;
		var temp3 = i + 2;
	}
}

// declare vars outside loop
function varOutside(){
	var temp;
	var temp2;
	var temp3;
	for(var i = 0; i < 100000000; i++){
		temp = i
		temp2 = i + 1
		temp3 = i + 2
	}
}

// for computing average execution times
var insideAvg = 0;
var outsideAvg = 0;

// run varInside a million times and average execution times
for(var i = 0; i < 1000; i++){
	var start = now()
	varInside()
	var end = now()
	insideAvg = (insideAvg + (end-start)) / 2
}

// run varOutside a million times and average execution times
for(var i = 0; i < 1000; i++){
	var start = now()
	varOutside()
	var end = now()
	outsideAvg = (outsideAvg + (end-start)) / 2
}

console.log('declared inside loop', insideAvg)
console.log('declared outside loop', outsideAvg)