// I am seeing what happens when I assign a function
// to a variable without using the invocation operator ()

// OK, so you can assign a function to a var and then invoke
// the var just like it's the function

function funky(){
	return 5;
}

console.log('assigning without ()')
var var1 = funky
console.log('var1', var1)

console.log('assigning with ()')
var var2 = funky()
console.log('var2', var2)

console.log('trying to invoke var1')
var1 = var1()
console.log('var1 again', var1)